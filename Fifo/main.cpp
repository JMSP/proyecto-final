#include <iostream>
#include <fstream>
#include <iostream>
#include <stdlib.h>

/*void guardarDatosArreglo(int * _arreglo){

    std::ifstream entrada("lista.txt");
    std::string lineaDeTexto;
    int cont = 0;

    while(getline(entrada, lineaDeTexto)){

        _arreglo[cont] = atoi(lineaDeTexto.c_str());
        cont += 1;

    }

    entrada.close();

}*/

void guardarDatosArreglo(int * _arreglo){

    int i = 0;

    while(1){

        if(feof(stdin))
            break;

        fscanf(stdin, "%d", &_arreglo[i++]);

    }

}

bool buscar(int * _arreglo, int _tamArreglo, int _elemento){

    for(int i = 0; i < _tamArreglo; i++)
        if(_arreglo[i] == _elemento)
            return true;

    return false;

}

int main(int argc, char * argv[]){

    int tamArreglo = atoi(argv[1]);
    int tamArregloFrame = atoi(argv[2]);

    int * arreglo = new int[tamArreglo];
    int * arregloFrame = new int[tamArregloFrame];
    int cont = 0, contPaginas = 0, a = 0;

    guardarDatosArreglo(arreglo);

    for(int i = 0; i < tamArregloFrame; i++)
        arregloFrame[i] = -1;

    for(int i = 0; i < tamArreglo; i++)
        std::cout << arreglo[i] << " ";

    std::cout << "\n";

    for(int i = 0; i < tamArreglo; i++){

        bool encontrado = buscar(arregloFrame, tamArregloFrame, arreglo[i]);

        if(!encontrado){

            contPaginas += 1;
            cont = a % tamArregloFrame; // Variable que indica donde se guardara el valor en arregloFrame.
            arregloFrame[cont] = arreglo[i];
            a += 1;

            std::cout << "\n";
            for(int j = 0; j < tamArregloFrame; j++)
                std::cout << i << " :: " << arregloFrame[j] << "\t\t";
            std::cout << "\n";

        }
    }

    delete [] arreglo;
    delete [] arregloFrame;

    std::cout << "\n";
    std::cout << "Num. paginas: " << contPaginas;
    std::cout << "\n";

    return 0;

}
