#include <iostream>
#include <fstream>
#include <iostream>
#include <stdlib.h>

void guardarDatosArreglo(int * _arreglo){

  std::ifstream entrada("lista.txt");
  std::string lineaDeTexto;
  int cont = 0;

  while(getline(entrada, lineaDeTexto)){

      _arreglo[cont] = atoi(lineaDeTexto.c_str());
      cont += 1;

  }

  entrada.close();

}

int main(int argc, char * argv[]){

  int numPaginas = atoi(argv[1]);
  int * arregloPaginas = new int[numPaginas];

  guardarDatosArreglo(arregloPaginas);

  for(int i = 0; i < numPaginas; i++) 
    printf("%d\n", arregloPaginas[i]);
   
  printf("\n");

  return 0;

}
