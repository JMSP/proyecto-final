#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){

   int limite = atoi(argv[1]), seed = atoi(argv[2]), min = atoi(argv[3]), max= atoi(argv[4]);

   srand(seed);
   int range = max - min;

   for(int i = 0; i < limite; i++)
     printf("%d\n", min+rand()%range);
   
   printf("\n");

}
