#include <iostream>
#include <fstream>
#include <iostream>
#include <stdlib.h>

void guardarDatosArreglo(int * _arreglo){

    std::ifstream entrada("lista.txt");
    std::string lineaDeTexto;
    int cont = 0;

    while(getline(entrada, lineaDeTexto)){

        _arreglo[cont] = atoi(lineaDeTexto.c_str());
        cont += 1;

    }

    entrada.close();

}

bool buscar(int * _arreglo, bool * _arregloBits, int _tamArreglo, int _elemento){

    for(int i = 0; i < _tamArreglo; i++)
        if(_arreglo[i] == _elemento){

            _arregloBits[i] = true;
            return true;

        }

    return false;

}

int main(){

    int tamArreglo = 18;
    int tamArregloFrame = 3;

    int * arreglo = new int[tamArreglo];
    int * arregloFrame = new int[tamArregloFrame];
    bool * arregloBits = new bool[tamArregloFrame];
    int cont = 0, contPaginas = 0, a = 0;

    guardarDatosArreglo(arreglo);

    for(int i = 0; i < tamArregloFrame; i++){

        arregloFrame[i] = -1;
        arregloBits[i] = false;

    }

    for(int i = 0; i < tamArreglo; i++)
        std::cout << arreglo[i] << " ";

    std::cout << "\n";

    for(int i = 0; i < tamArreglo; i++){

        bool encontrado = buscar(arregloFrame, arregloBits, tamArregloFrame, arreglo[i]);

        if(!encontrado){

            cont = a % tamArregloFrame;

            for(int j = 0, b = cont; j <= tamArregloFrame; j++, b++){

                if(!arregloBits[b % tamArregloFrame]){

                    contPaginas += 1;
                    //cont = a % tamArregloFrame; // Variable que indica donde se guardara el valor en arregloFrame.
                    arregloFrame[cont] = arreglo[i];
                    arregloBits[b % tamArregloFrame] = true;
                    a += 1;

                    break;

                }

                else
                    arregloBits[b % tamArregloFrame] = false;

            }

            std::cout << "\n";
            for(int j = 0; j < tamArregloFrame; j++)
                std::cout << i << " :: " << arregloFrame[j] << "\t\t";
            std::cout << "\n";

        }

        else
            std::cout << "\n-- "; // Sin reemplazo.

        for(int k = 0; k < tamArregloFrame; k++)
            std::cout << i << " :: " << arregloBits[k] << "\t\t";
        std::cout << "\n";

    }

    delete [] arreglo;
    delete [] arregloFrame;
    delete [] arregloBits;

    std::cout << "\n";
    std::cout << "Num. paginas: " << contPaginas;
    std::cout << "\n";

    return 0;

}
