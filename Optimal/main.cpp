#include <iostream>
#include <fstream>
#include <iostream>
#include <stdlib.h>

/*void guardarDatosArreglo(int * _arreglo){

    std::ifstream entrada("lista.txt");
    std::string lineaDeTexto;
    int cont = 0;

    while(getline(entrada, lineaDeTexto)){

        _arreglo[cont] = atoi(lineaDeTexto.c_str());
        cont += 1;

    }

    entrada.close();

}*/

void guardarDatosArreglo(int * _arreglo){

    int i = 0;

    while(1){

        if(feof(stdin))
            break;

        fscanf(stdin, "%d", &_arreglo[i++]);

    }

}

bool buscar(int * _arreglo, int _tamArreglo, int _elemento){

    for(int i = 0; i < _tamArreglo; i++)
        if(_arreglo[i] == _elemento)
            return true;

    return false;

}


int buscarMayor(int * _arreglo, int * _arregloFrame, int _tamArreglo, int _tamArregloFrame, int _posActual){

    // Verificar si el valor se encuentra m�s adelante en la lista.

    bool encontrado;

    for(int i = 0; i < _tamArregloFrame; i++){

        encontrado = false;

        for(int j = _posActual + 1; j <= _tamArreglo; j++){

            if(_arregloFrame[i] == _arreglo[j])
                encontrado = true;

        }

        // El primero que no encuentra es la posicion a cambiar.
        if(!encontrado)
            return i;

    }

    // Verificar si el valor se encuentra m�s adelante en la lista.

    // Si todos los valores se encontraron en la lista vemos cual es el m�s lejano.

    if(encontrado){

        int mayor = 0;

        std::cout << "\n";

        for(int i = 0; i < _tamArregloFrame; i++){

            int cont = 0;

            for(int j = _posActual + 1; j < _tamArreglo; j++){

                cont += 1;

                if(_arregloFrame[i] == _arreglo[j])
                    break;

            }

            std::cout << "Distancia: " << cont << "\n";

            if(cont > mayor)
                mayor = cont;

        }

        for(int i = 0; i < _tamArregloFrame; i++)
            if(_arregloFrame[i] == _arreglo[_posActual + mayor])
                return i;

    }

    // Si todos los valores se encontraron en la lista vemos cual es el m�s lejano.

}

int main(int argc, char * argv[]){

    int tamArreglo = atoi(argv[1]);
    int tamArregloFrame = atoi(argv[2]);

    int * arreglo = new int[tamArreglo];
    int * arregloFrame = new int[tamArregloFrame];
    int cont = 0, contPaginas = 0;

    guardarDatosArreglo(arreglo);

    for(int i = 0; i < tamArregloFrame; i++)
        arregloFrame[i] = -1;

    for(int i = 0; i < tamArreglo; i++)
        std::cout << arreglo[i] << " ";

    std::cout << "\n";

    for(int i = 0; i < tamArreglo; i++){

        bool encontrado = buscar(arregloFrame, tamArregloFrame, arreglo[i]);

        //Los primeros N valores se agregan a las lista de frames. N = Tama�o de la lista Frames.

        if(i < tamArregloFrame && !encontrado){

            arregloFrame[i % tamArregloFrame] = arreglo[i];
            contPaginas += 1;

            std::cout << "\n";
            for(int j = 0; j < tamArregloFrame; j++)
            std::cout << i << " :: " << arregloFrame[j] << "\t\t";
            std::cout << "\n";

        }

        //Los primeros N valores se agregan a las lista de frames. N = Tama�o de la lista Frames.

        else{

            if(!encontrado){

                int posicion = buscarMayor(arreglo, arregloFrame, tamArreglo, tamArregloFrame, i);

                contPaginas += 1;
                arregloFrame[posicion] = arreglo[i];

                std::cout << "\n";
                for(int j = 0; j < tamArregloFrame; j++)
                std::cout << i << " :: " << arregloFrame[j] << "\t\t";
                std::cout << "\n";

            }
        }
    }

    delete [] arreglo;
    delete [] arregloFrame;

    std::cout << "\n";
    std::cout << "Num. paginas: " << contPaginas;
    std::cout << "\n";

    return 0;

}
